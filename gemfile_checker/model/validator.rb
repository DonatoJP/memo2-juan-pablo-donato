class Validator

  def process(gemfile_content)
    if gemfile_content.empty?
      return false
    end

    lines = gemfile_content.strip.split("\n")

    if !check_source(lines[0])
      return false
    end

    lines = lines.drop(1)
    ruby_checked = false
    last_line = ""

    lines.each do |line|

      if line.empty?
        next
      end

      if !ruby_checked && !check_ruby(line)
        return false
      else
        ruby_checked = true
      end

      if line.split[0].eql? "gem"

        if last_line.empty?
          last_line = line
          next
        end

        if !compare_gems(line, last_line)
          return false
        else
          last_gem = line
        end

      end

    end
    return true
  end

  private

    def check_source(line)
      if line.split[0].eql? "source"
        return true
      end

      false
    end

    def check_ruby(line)
      if line.split[0].eql? "ruby"
        return true
      end

      false
    end

    def compare_gems(actual_line, last_line)
      actual_gem = actual_line.split[1]
      last_gem = last_line.split[1]

      min_len = (actual_gem.size < last_gem.size) ? actual_gem.size : last_gem.size

      i = 0

      while ((i < min_len-1) && (actual_gem[i] == last_gem[i])) do
        i += 1
      end

      return actual_gem[i] > last_gem[i]
    end
end
